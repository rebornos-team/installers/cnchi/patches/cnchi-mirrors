# cnchi-mirrors

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

To correct cnchi reborn-mirrorlist and mirrorlist
<br><br>
NOTE: This package loads mirrors from raw, from a location where the list is updated. Therefore, when downloading the repository, before running makepkg, run updpkgsums to update the checksums, and update pkgver. Then compile it.
<br><br>
How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/installers/cnchi/patches/cnchi-mirrors.git
```
<br><br>
**Arch Linux mirrorlist raw:**

```
https://gitlab.com/rebornos-team/rebornos-special-system-files/mirrors/mirrorlist/-/raw/master/mirrorlist
```
<br><br>
**reborn-mirrorlist raw:**

```
https://gitlab.com/rebornos-team/rebornos-special-system-files/mirrors/reborn-mirrorlist/-/raw/master/reborn-mirrorlist
```
